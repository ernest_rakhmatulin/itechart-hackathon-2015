__all__ = ['BaseManager']

from collections import Iterable

from django.db import models
from django.core.cache import cache


class BaseManager(models.Manager):

    def get_cache_key(self, objs):
        item = objs[0] if isinstance(objs, Iterable) else self
        return "{}".format(str(item.__class__))

    def get_all(self):
        return self.all()

    def get_id_map(self, objs=None, force=False):
        data = cache.get(self.get_cache_key(objs))
        if data is None or force:
            data = {
                obj.id: obj
                for obj in (objs if objs is not None else self.get_all())
            }
        return data
