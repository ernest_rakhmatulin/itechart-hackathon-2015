__all__ = ['add_months', 'dates_compare', 'ensure_date', 'ensure_timezone']


from datetime import datetime, date

from django.utils import timezone
import pytz


def add_months(dt, number_of_months, day=None, hour=None, minute=None,
               second=None, microsecond=None):
    total_months = dt.year * 12 + dt.month - 1 + number_of_months
    if isinstance(dt, date):
        return date(
            total_months / 12,
            total_months % 12 + 1,
            dt.day if day is None else day
        )
    return datetime(
        total_months / 12,
        total_months % 12 + 1,
        dt.day if day is None else day,
        dt.hour if hour is None else hour,
        dt.minute if minute is None else minute,
        dt.second if second is None else second,
        dt.microsecond if microsecond is None else microsecond
    )


def dates_compare(dt1, dt2):
    dt1 = ensure_date(dt1)
    dt2 = ensure_date(dt2)
    if dt1 > dt2:
        return 1
    elif dt1 == dt2:
        return 0
    return -1


def ensure_date(dt1):
    return dt1.date() if isinstance(dt1, datetime) else dt1


def ensure_timezone(dt):
    if timezone.is_naive(dt):
        return dt.replace(tzinfo=pytz.UTC)
    return dt.astimezone(pytz.UTC)
