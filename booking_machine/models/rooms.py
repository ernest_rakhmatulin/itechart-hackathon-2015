__all__ = ['RoomAttributes', 'Room']


from django.db import models

from booking_machine.utils import BaseManager, cached_method


class RoomAttributesManager(BaseManager):
    pass


class RoomAttributes(models.Model):
    name = models.CharField(max_length=255)

    objects = RoomAttributesManager()

    def __unicode__(self):
        return self.name

class RoomManager(BaseManager):
    def get_all(self):
        return self.all().select_related('attrs')


class Room(models.Model):
    name = models.CharField(max_length=255)
    attrs = models.ManyToManyField(RoomAttributes, blank=True, null=True)
    bookable = models.BooleanField(default=False)
    capacity = models.IntegerField()

    objects = RoomManager()

    def _get_attr_ids_set(self):
        return {
            str(attr.id)
            for attr in self.attrs.all()
        }

    get_attrs_ids_set = cached_method(
        _get_attr_ids_set,
        cache_key='room_attrs',
        in_first_arg=True
    )

    def has_attrs(self, attr_ids):
        return not bool(self.get_missed_attrs(attr_ids))

    def get_missed_attrs(self, attr_ids):
        attr_ids = set(str(i) for i in attr_ids)
        room_attrs = self.get_attrs_ids_set()
        return attr_ids.difference(room_attrs)

    def __unicode__(self):
        return self.name
