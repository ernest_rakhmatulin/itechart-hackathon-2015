from enum import Enum


class BaseEnum(Enum):
    def __init__(self, *args):
        self.verbose_name, self._value_ = args[:2]
        self._value2member_map_[self._value_] = self

    @classmethod
    def get_choices(cls):
        return tuple((i._value_, i.verbose_name) for i in cls)


class ReservationStates(BaseEnum):
    RESERVED = 'Reserved', 0  # green
    PENDING = 'Pending', 1  # gray
    COLLISION = 'Collision', 2  # red
