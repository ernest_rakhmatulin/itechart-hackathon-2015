from rest_framework import serializers
from models.users import Profile
from django.contrib.auth.models import User
from models.rooms import RoomAttributes


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('id', 'user', 'location')


class UserSerializer(serializers.ModelSerializer):
    locations = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'locations')

    def get_locations(self, obj):
        return {
            'default_location': obj.profile.get_default_location(),
            'current_location': obj.profile.get_current_location(),
        }


class RoomAttributesSerializer(serializers.ModelSerializer):
    class Meta:
        model = RoomAttributes


class RoomAttributeSerilizer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()


class RoomAttributeField(serializers.Field):
    def to_representation(self, room):
        attributes = room.attrs.all()
        return RoomAttributeSerilizer(attributes, many=True).data


class AttendeesField(serializers.Field):
    def to_representation(self, reservation):
        attendees_list = reservation.attendees.all()
        return AttendeeSerializer(attendees_list, many=True).data


class AttendeeSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.user.username

    def get_email(self, obj):
        return obj.user.email


class AttendeesField(serializers.Field):
    def to_representation(self, reservation):
        attendees_list = reservation.attendees.all()
        return AttendeeSerializer(attendees_list, many=True).data


class RoomSerializer(serializers.Serializer):
    id = serializers.CharField()
    name = serializers.CharField()
    capacity = serializers.IntegerField()
    attrs = RoomAttributeField(source='*')

    def get_name(self, obj):
        print obj


class ReservationSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    room = RoomSerializer()
    attendees = AttendeesField(source='*')
    subject = serializers.CharField()
    description = serializers.CharField(max_length=140, allow_blank=True, allow_null=True)
    start_date = serializers.SerializerMethodField()
    end_date = serializers.SerializerMethodField()
    state = serializers.IntegerField()
    series_id = serializers.CharField()

    def get_start_date(self, obj):
        return obj.start_time

    def get_end_date(self, obj):
        return obj.end_time


class PostRoomAttributesField(serializers.Field):
    def to_internal_value(self, data):
        return data

class PostReservationSerializer(serializers.Serializer):
    attendees = serializers.ListField(child=serializers.CharField())
    subject = serializers.CharField()
    description = serializers.CharField(max_length=140, allow_blank=True, allow_null=True)
    start_time = serializers.TimeField()
    end_time = serializers.TimeField()
    interval = serializers.IntegerField()
    start_date = serializers.DateField(input_formats=['%m-%d-%Y'])
    end_date = serializers.DateField(input_formats=['%m-%d-%Y'])
    room_attrs = PostRoomAttributesField()

