from django.contrib import admin
from models.users import *
from models.reservations import *
from models.rooms import *

admin.site.register(Profile)
admin.site.register(Reservation)
admin.site.register(ReservationsSeries)
admin.site.register(Room)
admin.site.register(RoomAttributes)