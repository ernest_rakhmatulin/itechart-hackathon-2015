window.utils = window.utils || (function($, _) {
    var renderTemplate = function(template, data) {
        var html = template.html();
        //console.log(html);
        var compiler = _.template(html);
        //console.log(compiler({'name': 'damn it'}));
        return compiler(data);
    };

    var initTypehead = function(data, name, $selector, additionalOptions) {
        var substringMatcher = function(strs) {
          return function findMatches(q, cb) {
            var matches, substrRegex;
            matches = [];
            substrRegex = new RegExp(q, 'i');
            $.each(strs, function(i, str) {
              if (substrRegex.test(str)) {
                matches.push(str);
              }
            });
            cb(matches);
          };
        };

        var options = {
            hint: true,
            hightlight: true,
            minLength: 1
        };

        $selector.typeahead(
            $.extend(options, additionalOptions || {})
            , {
                name: name,
                source: substringMatcher(data)
            }
        );
    };

    var showButtons = function() {
        var $this = $(this);
        $this.find('.remove-button').velocity('stop', true).velocity('transition.bounceIn', {duration: 300});
        $this.find('.show-button').velocity('stop', true).velocity('transition.bounceIn', {duration: 300});
    };

    var removeButtons = function() {
        var $this = $(this);
            $this.find('.remove-button').velocity('stop', true).velocity('transition.bounceOut', {duration: 300});
            $this.find('.show-button').velocity('stop', true).velocity('transition.bounceOut', {duration: 300});
    };

    var basisCalendarOptions = {
        weekends: false,
        header: {
            left: 'prev, next today',
            center: 'title',
            right: 'month, agendaWeek, agendaDay'
        },
        firstDay: 1,
        allDayText: 'all day event',
        minTime: "08:30:00",
        maxTime: "22:00:00",
        slotEventOverlap: false,
        eventDragStart: function() {
            showButtons.call(this);
        },
        eventDragEnd: function() {
            removeButtons.call(this);
        },
        eventMouseover: function() {
            showButtons.call(this);
        },
        eventMouseout: function() {
            removeButtons.call(this);
        }
    };

    return {
        basicConfig: basisCalendarOptions,
        showButtons: showButtons,
        removeButtons: removeButtons,
        initTypeahead: initTypehead,
        renderTemplate: renderTemplate
    }
})(jQuery, _);