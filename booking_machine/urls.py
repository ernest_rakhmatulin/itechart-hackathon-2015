"""booking_machine URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from django.conf.urls import url, include
from rest_framework import routers
from views import ProfileViewSet, ReservationsView, SeriesView, \
    UsersViewSet, RoomsViewSet, RoomAttributesViewSet

router = routers.SimpleRouter()
router.register(r'profiles', ProfileViewSet)
router.register(r'users', UsersViewSet)
router.register(r'rooms', RoomsViewSet)
router.register(r'attrs', RoomAttributesViewSet)

urlpatterns = router.urls

urlpatterns = [
    url('^s$', 'booking_machine.views.search_view', name='search'),
    url('^$', 'booking_machine.views.basic_view', name='main'),
    url(r'^api/', include(router.urls)),
    url(r'^api/reservations/(?P<pk>\w+)?', ReservationsView.as_view(), name='reservations-list'),
    url(r'^api/series/(?P<pk>\w+)?', SeriesView.as_view(), name='series'),
    url(r'^admin/', include(admin.site.urls)),
]

