# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(default=b'', max_length=255)),
                ('description', models.TextField(default=b'')),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
                ('state', models.IntegerField(choices=[(2, b'Collision'), (1, b'Pending'), (0, b'Reserved')])),
                ('attendees', models.ManyToManyField(to='booking_machine.Profile')),
            ],
        ),
        migrations.CreateModel(
            name='ReservationsSeries',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('bookable', models.BooleanField(default=False)),
                ('capacity', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='RoomAttributes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='room',
            name='attrs',
            field=models.ManyToManyField(to='booking_machine.RoomAttributes', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='reservation',
            name='room',
            field=models.ForeignKey(related_name='reservations', to='booking_machine.Room'),
        ),
        migrations.AddField(
            model_name='reservation',
            name='series',
            field=models.ForeignKey(to='booking_machine.ReservationsSeries'),
        ),
        migrations.AddField(
            model_name='profile',
            name='location',
            field=models.ForeignKey(blank=True, to='booking_machine.Room', null=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
    ]
